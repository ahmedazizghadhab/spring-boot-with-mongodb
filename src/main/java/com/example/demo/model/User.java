package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString

@Document(collection = "User")
public class User {
    @Id
    private int id ;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String email;
    private String mobileNumber;
    private String role;
    private String historique;
    @JsonFormat(pattern="yyyy-dd-MM HH:mm")
    LocalDateTime  rdv;


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /* public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        userName = userName;
    } */

    public String getPassword() { return password; }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public String getHistorique() {
        return historique;
    }
    public void setHistorique(String historique) {
        this.historique = historique;
    }

    public LocalDateTime getRdv() {
        return rdv;
    }
    public void setRdv(LocalDateTime  rdv) {
        this.rdv = rdv;
    }
}
