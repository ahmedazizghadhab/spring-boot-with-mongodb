package com.example.demo.resource;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/sayHello")
    public String sayHello(){
        return "hello world !";
    }

    @PostMapping("/addUser")
    public String saveUser(@RequestBody User user){
        userRepository.save(user);
        return "Added user with id: " + user.getId();
    }

    @GetMapping("/findAllUsers")
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @GetMapping("/findUser/{id}")
    public Optional<User> getUser(@PathVariable int id){ return userRepository.findById(id); }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable int id){
        userRepository.deleteById(id);
        return "user deleted with id: " +id;
    }
}



